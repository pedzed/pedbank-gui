-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema saatbank
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema saatbank
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `saatbank` DEFAULT CHARACTER SET utf8 ;
USE `saatbank` ;

-- -----------------------------------------------------
-- Table `saatbank`.`log`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `saatbank`.`log` (
  `log_id` INT NOT NULL AUTO_INCREMENT,
  `message` TEXT NOT NULL,
  `timedate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `atm_id` VARCHAR(8) NOT NULL,
  PRIMARY KEY (`log_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `saatbank`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `saatbank`.`user` (
  `user_id` INT NOT NULL AUTO_INCREMENT,
  `initials` VARCHAR(32) NOT NULL,
  `surname` VARCHAR(64) NOT NULL,
  PRIMARY KEY (`user_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `saatbank`.`card`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `saatbank`.`card` (
  `nuid` VARCHAR(8) NOT NULL,
  `user_id` INT NOT NULL,
  `pincode` VARCHAR(16) NOT NULL,
  `attempts` INT NOT NULL,
  PRIMARY KEY (`nuid`, `user_id`),
  INDEX `fk_cards_members1_idx` (`user_id` ASC),
  CONSTRAINT `fk_cards_members1`
    FOREIGN KEY (`user_id`)
    REFERENCES `saatbank`.`user` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `saatbank`.`account`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `saatbank`.`account` (
  `iban` VARCHAR(16) NOT NULL,
  `balance` INT NOT NULL,
  PRIMARY KEY (`iban`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `saatbank`.`members_accounts`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `saatbank`.`members_accounts` (
  `members_id` INT NOT NULL,
  `accounts_id` INT NOT NULL,
  PRIMARY KEY (`members_id`, `accounts_id`),
  INDEX `fk_members_has_accounts_members_idx` (`members_id` ASC),
  CONSTRAINT `fk_members_has_accounts_members`
    FOREIGN KEY (`members_id`)
    REFERENCES `saatbank`.`user` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `saatbank`.`cardaccount`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `saatbank`.`cardaccount` (
  `nuid` VARCHAR(8) NOT NULL,
  `iban` VARCHAR(16) NOT NULL,
  PRIMARY KEY (`nuid`, `iban`),
  INDEX `fk_accounts_cards_account1_idx` (`iban` ASC),
  INDEX `fk_accounts_cards_cards1_idx` (`nuid` ASC),
  CONSTRAINT `fk_accounts_cards_account1`
    FOREIGN KEY (`iban`)
    REFERENCES `saatbank`.`account` (`iban`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_accounts_cards_cards1`
    FOREIGN KEY (`nuid`)
    REFERENCES `saatbank`.`card` (`nuid`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `saatbank`.`useraccount`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `saatbank`.`useraccount` (
  `user_id` INT NOT NULL,
  `iban` VARCHAR(16) NOT NULL,
  PRIMARY KEY (`user_id`, `iban`),
  INDEX `fk_members_has_accounts_members1_idx` (`user_id` ASC),
  INDEX `fk_useraccount_account1_idx` (`iban` ASC),
  CONSTRAINT `fk_members_has_accounts_members1`
    FOREIGN KEY (`user_id`)
    REFERENCES `saatbank`.`user` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_useraccount_account1`
    FOREIGN KEY (`iban`)
    REFERENCES `saatbank`.`account` (`iban`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `saatbank`.`transaction`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `saatbank`.`transaction` (
  `transaction_id` INT NOT NULL AUTO_INCREMENT,
  `iban` VARCHAR(16) NOT NULL,
  `amount` INT NOT NULL,
  `atm_id` VARCHAR(8) NOT NULL,
  `timedata` TIMESTAMP NOT NULL,
  PRIMARY KEY (`transaction_id`, `iban`),
  INDEX `fk_transaction_account1_idx` (`iban` ASC),
  CONSTRAINT `fk_transaction_account1`
    FOREIGN KEY (`iban`)
    REFERENCES `saatbank`.`account` (`iban`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

