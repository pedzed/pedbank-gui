package pedbank;

import javafx.application.Application;
import javafx.stage.Stage;
import pedbank.Controllers.WelcomeController;
import pedbank.Exceptions.DatabaseException;

public class PedBank extends Application 
{
    private static App app = App.getInstance();

    public static void main(String[] args) throws DatabaseException
    {
        app.database = new Database();
        app.sceneSwitcher = new SceneSwitcher();

        Microcontroller microcontroller = new Microcontroller();
        microcontroller.handleSerial();

        launch(args);
    }

    @Override
    public void start(Stage stage)
    {
        app.sceneSwitcher.setStage(stage);

        WelcomeController controller = new WelcomeController();
        app.sceneSwitcher.setController(controller);
        controller.index();

        stage.setTitle("PedBank - The Bank of Ped");
        stage.show();
    }
}
