package pedbank.Models;

import pedbank.Exceptions.ModelNotFoundException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class User extends Model
{
    public int user_id;
    public String initials;
    public String surname;

    public static User find(int id) throws ModelNotFoundException
    {
        try {
            String sql =
                "SELECT * " +
                "FROM `user` " +
                "WHERE `user_id` = ?"
            ;
            PreparedStatement statement = database.prepareStatement(sql);
            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();

            if (!resultSet.next()) {
                throw new ModelNotFoundException("User not found in database.");
            }

            return userFromResultSet(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    private static User userFromResultSet(ResultSet resultSet) throws SQLException
    {
        User user = new User();

        user.user_id = resultSet.getInt("user_id");
        user.initials = resultSet.getString("initials");
        user.surname = resultSet.getString("surname");

        return user;
    }

    @Override
    public boolean save()
    {
        //
        return false;
    }
}
