package pedbank.Models;

import pedbank.App;

import java.sql.Connection;

public abstract class Model
{
    protected static Connection database = App.getInstance().database.getConnection();

    abstract public boolean save();
}
