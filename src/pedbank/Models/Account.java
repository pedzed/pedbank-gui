package pedbank.Models;

import pedbank.Exceptions.ModelNotFoundException;
import pedbank.Exceptions.WithdrawException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedHashSet;

public class Account extends Model
{
    public String iban;
    public double balance;

    public void withdraw(double withdrawalAmount) throws WithdrawException
    {
        //TODO: Clean up
        if (withdrawalAmount < 10 || withdrawalAmount > 1000) {
            throw new WithdrawException("Withdrawal amount must be between €10 and €1000.");
        }

        if (withdrawalAmount % 10 != 0) {
            throw new WithdrawException("Withdrawal amount must be a factor of 10.");
        }

        double newBalance = balance - withdrawalAmount;

        if (newBalance < 0) {
            throw new WithdrawException("Insufficient money.");
        }

        createTransaction(withdrawalAmount);

        balance = newBalance;
        save();
    }

    private void createTransaction(double withdrawalAmount)
    {
        Transaction transaction = new Transaction();
        transaction.iban = iban;
        transaction.amount = -1 * withdrawalAmount;
        transaction.atm_id = "PEDB";
        transaction.save();
    }

    @Override
    public boolean save()
    {
        try {
            String sql =
                "UPDATE `account` " +
                "SET `balance` = ?" +
                "WHERE `account`.`iban` = ?"
            ;
            PreparedStatement statement = database.prepareStatement(sql);
            statement.setDouble(1, balance);
            statement.setString(2, iban);

            return (statement.executeUpdate() == 1);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return false;
    }

    public static LinkedHashSet<Account> findAllByCard(String nuid) throws ModelNotFoundException
    {
        try {
            String sql =
                "SELECT `account`.* " +
                "FROM `account`, `cardaccount` " +
                "WHERE `account`.`iban` = `cardaccount`.`iban`" +
                "AND `cardaccount`.`nuid` = ?"
            ;
            PreparedStatement statement = database.prepareStatement(sql);
            statement.setString(1, nuid);
            ResultSet resultSet = statement.executeQuery();

            LinkedHashSet<Account> accounts = new LinkedHashSet<Account>();

            while (resultSet.next()) {
                Account account = accountFromResultSet(resultSet);
                accounts.add(account);
            }

            if (accounts.size() == 0) {
                throw new ModelNotFoundException("Account(s) not found in database.");
            }

            return accounts;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    private static Account accountFromResultSet(ResultSet resultSet) throws SQLException
    {
        Account account = new Account();

        account.iban = resultSet.getString("iban");
        account.balance = resultSet.getDouble("balance");

        return account;
    }
}
