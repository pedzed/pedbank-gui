package pedbank.Models;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public class Transaction extends Model
{
    public int transaction_id;
    public String iban;
    public double amount;
    public String atm_id;
    public String timedate;

    @Override
    public boolean save()
    {
        try {
            String sql =
                "INSERT INTO `transaction` (" +
                    "`iban`, " +
                    "`amount`, " +
                    "`atm_id`" +
                ") VALUES (" +
                    "?, " +
                    "?, " +
                    "? " +
                ")"
            ;
            PreparedStatement statement = database.prepareStatement(sql);
            statement.setString(1, iban);
            statement.setDouble(2, amount);
            statement.setString(3, atm_id);

            return (statement.executeUpdate() == 1);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return false;
    }
}
