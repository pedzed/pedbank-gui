package pedbank.Models;

import pedbank.Exceptions.AuthException;
import pedbank.Exceptions.ModelNotFoundException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedHashSet;

public class Card extends Model
{
    final public static int MIN_PINCODE_SIZE = 4;
    final public static int MAX_PINCODE_SIZE = 4;
    final public static int MAX_AUTH_ATTEMPTS = 3;

    public String nuid;
    public int user_id;
    public int pincode;
    public int attempts;

    public User user;
    public LinkedHashSet<Account> accounts;

    public static Card find(String nuid) throws ModelNotFoundException
    {
        try {
            String sql =
                "SELECT * " +
                "FROM `card` " +
                "WHERE `nuid` = LOWER(?)"
            ;
            PreparedStatement statement = database.prepareStatement(sql);
            statement.setString(1, nuid);
            ResultSet resultSet = statement.executeQuery();

            if (!resultSet.next()) {
                throw new ModelNotFoundException("Card not found in database.");
            }

            Card card = cardFromResultSet(resultSet);
            card.user = User.find(card.user_id);
            card.accounts = Account.findAllByCard(card.nuid);

            return card;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    private static Card cardFromResultSet(ResultSet resultSet) throws SQLException
    {
        Card card = new Card();

        card.nuid = resultSet.getString("nuid");
        card.user_id = resultSet.getInt("user_id");
        card.pincode = resultSet.getInt("pincode");
        card.attempts = resultSet.getInt("attempts");

        return card;
    }

    public int attemptsLeft()
    {
        return (MAX_AUTH_ATTEMPTS - attempts);
    }

    public boolean isBlocked()
    {
        return (attemptsLeft() <= 0);
    }

    public void auth(int pincode) throws AuthException
    {
        //TODO: Hash pincode for security
        boolean isValidPincode = (pincode == this.pincode);

        if (isValidPincode) {
            attempts = 0;
            save();
            return;
        }

        attempts++;
        save();

        String error = "Entered wrong pincode.";

        if (attemptsLeft() == 1) {
            error += " 1 attempt left.";
        } else {
            error += String.format(" %d attempts left.", attemptsLeft());
        }

        if (isBlocked()) {
            error += " Card blocked.";
        }

        throw new AuthException(error);
    }

    @Override
    public boolean save()
    {
        try {
            String sql =
                "UPDATE `card` " +
                "SET `attempts` = ? " +
                "WHERE `card`.`nuid` = ?"
            ;
            PreparedStatement statement = database.prepareStatement(sql);
            statement.setInt(1, attempts);
            statement.setString(2, nuid);

            return (statement.executeUpdate() == 1);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return false;
    }
}
