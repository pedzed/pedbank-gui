package pedbank.Models;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public class Log extends Model
{
    public int log_id;
    public String message;
    public String timedate;
    public String atm_id;

    @Override
    public boolean save()
    {
        try {
            String sql =
                "INSERT INTO `log` (" +
                    "`message`, " +
                    "`atm_id`" +
                ") VALUES (" +
                    "?, " +
                    "? " +
                ")"
            ;
            PreparedStatement statement = database.prepareStatement(sql);
            statement.setString(1, message);
            statement.setString(2, atm_id);

            return (statement.executeUpdate() == 1);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return false;
    }

    public static void createFromMessage(String message)
    {
        Log log = new Log();
        log.message = message;
        log.atm_id = "PEDB";
        log.save();
    }
}
