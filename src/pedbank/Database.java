package pedbank;

import pedbank.Config.DatabaseConfig;
import pedbank.Exceptions.DatabaseException;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Database
{
    private Connection connection;

    public Database() throws DatabaseException
    {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection(
                DatabaseConfig.URL,
                DatabaseConfig.USER,
                DatabaseConfig.PASSWORD
            );
        } catch (ClassNotFoundException e) {
            throw new DatabaseException("Failed to find database library.");
        } catch (SQLException e) {
            throw new DatabaseException("Failed to establish database connection.");
        }
    }

    public Connection getConnection()
    {
        return connection;
    }
}
