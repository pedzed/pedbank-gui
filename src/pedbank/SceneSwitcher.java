package pedbank;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import pedbank.Controllers.BaseController;

import java.io.IOException;


public class SceneSwitcher
{
    private Stage stage;
    private BaseController controller;

    public void setStage(Stage stage)
    {
        this.stage = stage;
    }

    public Stage getStage()
    {
        return stage;
    }

    public void setController(BaseController controller)
    {
        this.controller = controller;
    }

    public BaseController getController()
    {
        return controller;
    }

    public void render(View view)
    {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(view.getUrl());
            Parent root = fxmlLoader.load();

            stage.setScene(new Scene(root));
            stage.getScene().getRoot().requestFocus();

            setController(fxmlLoader.getController());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
