package pedbank;

import com.fazecast.jSerialComm.SerialPort;
import com.fazecast.jSerialComm.SerialPortDataListener;
import com.fazecast.jSerialComm.SerialPortEvent;
import pedbank.Exceptions.SerialPortException;

public class Microcontroller
{
    private final String SERIAL_PORT_NAME = "USB2.0-Serial";
    private final int BAUD_RATE = 9600;

    public final static String RFID_PREFIX = "[RFID]: ";
    public final static String KEYPAD_PREFIX = "[KEYPAD]: ";

    public final static String BACK_KEY = "C";
    public final static String EXIT_KEY = "D";

    public void handleSerial()
    {
        listSerialPorts();

        try {
            SerialPort serialCommunication = serialPort();

            serialCommunication.addDataListener(new SerialPortDataListener()
            {
                @Override
                public int getListeningEvents()
                {
                    return SerialPort.LISTENING_EVENT_DATA_AVAILABLE;
                }

                @Override
                public void serialEvent(SerialPortEvent serialPortEvent)
                {
                    if (serialPortEvent.getEventType() != SerialPort.LISTENING_EVENT_DATA_AVAILABLE) {
                        return;
                    }

                    byte incomingData[] = new byte[serialCommunication.bytesAvailable()];
                    serialCommunication.readBytes(incomingData, incomingData.length);

                    String receivedData = new String(incomingData);
                    System.out.print(receivedData);

                    App.getInstance().sceneSwitcher.getController().handleSerialData(receivedData);
                }
            });
        } catch (SerialPortException e) {
            e.printStackTrace();
        }
    }

    private void listSerialPorts()
    {
        System.out.println("Listing available serial ports...");

        for (SerialPort serialPort : SerialPort.getCommPorts()) {
            if (serialPort.getDescriptivePortName().equalsIgnoreCase(SERIAL_PORT_NAME)) {
                System.out.print(" -> ");
            } else {
                System.out.print("    ");
            }

            System.out.printf(
                "| %s: %s\n",
                serialPort.getSystemPortName(),
                serialPort.getDescriptivePortName()
            );
        }

        System.out.println();
    }

    private SerialPort serialPort() throws SerialPortException
    {
        for (SerialPort serialPort : SerialPort.getCommPorts()) {
            if (serialPort.getDescriptivePortName().equalsIgnoreCase(SERIAL_PORT_NAME)) {
                serialPort.setBaudRate(BAUD_RATE);
                serialPort.openPort();

                return serialPort;
            }
        }

        throw new SerialPortException("Serial port not found.");
    }
}
