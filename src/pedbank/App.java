package pedbank;

import pedbank.Models.Account;
import pedbank.Models.Card;

public class App
{
    private static App instance = new App();

    public SceneSwitcher sceneSwitcher;
    public Database database;
    public Card card;
    public Account selectedAccount;

    private App()
    {
        //
    }

    public static App getInstance()
    {
        return instance;
    }
}
