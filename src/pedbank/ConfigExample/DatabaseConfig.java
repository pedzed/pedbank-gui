package pedbank.ConfigExample;

public class DatabaseConfig extends Config
{
    public static final String URL = "jdbc:mysql://ADDRESS_HERE:PORT_HERE/DATABASE_HERE";
    public static final String USER = "USERNAME_HERE";
    public static final String PASSWORD = "PASSWORD_HERE";
}
