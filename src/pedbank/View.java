package pedbank;

import java.net.URL;

public class View
{
    private URL url;

    public View(String view)
    {
        setUrl(view);
    }

    public void setUrl(String view)
    {
        url = getClass().getResource("/pedbank/Views/" + view + ".fxml");
    }

    public URL getUrl()
    {
        return url;
    }
}
