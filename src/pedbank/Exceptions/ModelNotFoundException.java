package pedbank.Exceptions;

public class ModelNotFoundException extends Exception
{
    public ModelNotFoundException(String message)
    {
        super(message);
    }
}
