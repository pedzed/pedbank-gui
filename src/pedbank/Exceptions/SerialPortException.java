package pedbank.Exceptions;

public class SerialPortException extends Throwable
{
    public SerialPortException(String message)
    {
        super(message);
    }
}
