package pedbank.Exceptions;

public class DatabaseException extends Exception
{
    public DatabaseException(String message)
    {
        super(message);
    }
}
