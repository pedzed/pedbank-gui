package pedbank.Controllers;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.input.MouseEvent;
import pedbank.Controllers.NormalUser.ViewSelectionController;
import pedbank.Exceptions.AuthException;
import pedbank.Microcontroller;
import pedbank.Models.Card;
import pedbank.Models.Log;
import pedbank.View;

import java.net.URL;
import java.util.ResourceBundle;

public class AuthController extends BaseController implements Initializable
{
    private final static String CONFIRM_KEY = "A";
    private final static String CLEAR_KEY = "B";
    private final static String EXIT_KEY = "D";

    @FXML
    private PasswordField pincodeField;

    @FXML
    private Label noteLabel;

    public void index()
    {
        Platform.runLater(() -> {
            app.sceneSwitcher.setController(this);
            app.sceneSwitcher.render(new View("Auth"));
        });
    }

    @Override
    public void handleSerialData(String data)
    {
        if (!data.startsWith(Microcontroller.KEYPAD_PREFIX)) {
            return;
        }

        String enteredChar = data.replace(Microcontroller.KEYPAD_PREFIX, "");
        enteredChar = enteredChar.replace(System.getProperty("line.separator"), "");

        if (enteredChar.matches("[0-9]")) {
            String newPincode = pincodeField.getText() + enteredChar;
            pincodeField.setText(newPincode);

            return;
        }

        switch (enteredChar) {
            case CLEAR_KEY:
                Log.createFromMessage("[View:Auth][Client] Clearing pincode.");

                Platform.runLater(() -> {
                    pincodeField.setText("");
                });
            break;

            case CONFIRM_KEY:
                Log.createFromMessage("[View:Auth][Client] Authenticating.");
                authenticate();
            break;

            case EXIT_KEY:
                Log.createFromMessage("[View:Auth][Client] Quit session.");
                exitAndReturnCard();
            break;
        }
    }

    private void authenticate()
    {
        Card card = app.card;

        if (card.isBlocked()) {
            Log.createFromMessage("[View:Auth][Client][Error] Card is blocked.");

            Platform.runLater(() -> {
                noteLabel.setText("Card is blocked.");
            });
            return;
        }

        if (pincodeField.getLength() == 0) {
            Log.createFromMessage("[View:Auth][Client][Error] No pincode entered.");

            Platform.runLater(() -> {
                noteLabel.setText("No pincode entered.");
            });
            return;
        }

        if (pincodeField.getLength() < Card.MIN_PINCODE_SIZE) {
            Log.createFromMessage("[View:Auth][Client][Error] Entered too few characters.");

            Platform.runLater(() -> {
                noteLabel.setText("Entered less than " + Card.MIN_PINCODE_SIZE + " characters.");
            });
            return;
        }

        if (pincodeField.getLength() > Card.MAX_PINCODE_SIZE) {
            Log.createFromMessage("[View:Auth][Client][Error] Entered too many characters.");

            Platform.runLater(() -> {
                noteLabel.setText("Entered more than " + Card.MAX_PINCODE_SIZE + " characters.");
            });
            return;
        }

        try {
            int pincode = Integer.parseInt(pincodeField.getText());
            card.auth(pincode);
        } catch (AuthException e) {
            Log.createFromMessage("[View:Auth][Client][Error] Failed auth.");

            Platform.runLater(() -> {
                noteLabel.setText(e.getMessage());
                pincodeField.setText("");
            });
            return;
        }

        app.selectedAccount = card.accounts.iterator().next();

        ViewSelectionController controller = new ViewSelectionController();
        controller.index();
    }

    @FXML
    private void onPincodeAction(ActionEvent event)
    {
        //
    }

    @FXML
    private void onConfirmButtonClick(MouseEvent event)
    {
        authenticate();
    }

    @FXML
    private void onClearButtonClick(MouseEvent event)
    {
        pincodeField.setText("");
    }

    @FXML
    public void onExitAndReturnCardButtonClick(MouseEvent mouseEvent)
    {
        Log.createFromMessage("[View:Auth] Exited.");
        exitAndReturnCard();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        pincodeField.setOnAction(this::onPincodeAction);
        noteLabel.setText("");
    }
}
