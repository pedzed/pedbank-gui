package pedbank.Controllers;

import pedbank.App;

public abstract class BaseController
{
    protected App app = App.getInstance();

    public abstract void handleSerialData(String receivedData);

    protected void showThankYouView()
    {
        ThankYouController controller = new ThankYouController();
        controller.index();
    }

    protected void exitAndReturnCard()
    {
        app.selectedAccount = null;
        app.card = null;

        WelcomeController controller = new WelcomeController();
        controller.index();
    }
}
