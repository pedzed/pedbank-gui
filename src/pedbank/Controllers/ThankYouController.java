package pedbank.Controllers;

import javafx.animation.PauseTransition;
import javafx.application.Platform;
import javafx.fxml.Initializable;
import javafx.util.Duration;
import pedbank.Models.Log;
import pedbank.View;

import java.net.URL;
import java.util.ResourceBundle;

public class ThankYouController extends BaseController implements Initializable
{
    public void index()
    {
        Platform.runLater(() -> {
            app.sceneSwitcher.setController(this);
            app.sceneSwitcher.render(new View("ThankYou"));
        });

        PauseTransition viewTimeout = new PauseTransition(Duration.millis(1500));

        Log.createFromMessage("[View:ThankYou][Client] -> [View:Welcome]");

        viewTimeout.setOnFinished(event -> {
            exitAndReturnCard();
        });

        viewTimeout.play();
    }

    @Override
    public void handleSerialData(String receivedData)
    {
        //
    }

    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        //
    }
}
