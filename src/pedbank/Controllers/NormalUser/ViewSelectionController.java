package pedbank.Controllers.NormalUser;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import pedbank.Controllers.BaseController;
import pedbank.Controllers.WelcomeController;
import pedbank.Exceptions.WithdrawException;
import pedbank.Microcontroller;
import pedbank.Models.Account;
import pedbank.Models.Log;
import pedbank.Models.User;
import pedbank.View;

import java.net.URL;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.ResourceBundle;

public class ViewSelectionController extends BaseController implements Initializable
{
    public final static int QUICK_CASH_AMOUNT = 70;

    private final static String QUICK_CASH_KEY = "A";
    private final static String EXIT_KEY = "D";

    @FXML
    private Label userNameLabel;

    @FXML
    private Label account1IbanLabel;

    @FXML
    private Label account2TitleLabel;

    @FXML
    private Label account2IbanLabel;

    public void index()
    {
        Platform.runLater(() -> {
            app.sceneSwitcher.setController(this);
            app.sceneSwitcher.render(new View("NormalUser/ViewSelection"));
        });
    }

    @Override
    public void handleSerialData(String data)
    {
        if (!data.startsWith(Microcontroller.KEYPAD_PREFIX)) {
            return;
        }

        String enteredChar = data.replace(Microcontroller.KEYPAD_PREFIX, "");
        enteredChar = enteredChar.replace(System.getProperty("line.separator"), "");

        WelcomeController welcomeController = new WelcomeController();
        WithdrawCashController withdrawCashController = new WithdrawCashController();
        BalanceController balanceController = new BalanceController();

        switch (enteredChar) {
            case "1":
                Log.createFromMessage("[View:ViewSelection][Client] -> [View:Withdraw]");
                withdrawCashController.index();
            break;

            case "4":
                Log.createFromMessage("[View:ViewSelection][Client] -> [View:Balance]");
                balanceController.index();
            break;

            case QUICK_CASH_KEY:
                Log.createFromMessage("[View:ViewSelection][Client] Quickly withdrawing.");
                try {
                    withdrawCashController.withdraw(QUICK_CASH_AMOUNT);
                } catch (WithdrawException e) {
                    Log.createFromMessage("[View:ViewSelection][Client][Error] Failed quick withdraw.");

                    //TODO: Show error using GUI
                    e.printStackTrace();
                }
            break;

            case EXIT_KEY:
                Log.createFromMessage("[View:ViewSelection][Client] Quit session.");
                welcomeController.index();
            break;
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        User user = app.card.user;
        String name = user.initials + " " + user.surname;
        userNameLabel.setText(name);

        LinkedHashSet<Account> accounts = app.card.accounts;
        Iterator<Account> accountIterator = accounts.iterator();
        account1IbanLabel.setText(accountIterator.next().iban);

        if (accountIterator.hasNext()) {
            account2IbanLabel.setText(accountIterator.next().iban);
            account2TitleLabel.setDisable(false);
            account2IbanLabel.setDisable(false);
        }
    }

    @FXML
    public void onQuickCashButtonClick(MouseEvent mouseEvent)
    {

        System.out.println("Withdrawing €" + QUICK_CASH_AMOUNT + "...");

        //TODO: Withdraw

        showThankYouView();
    }

    @FXML
    public void onWithdrawCashButtonClick(MouseEvent mouseEvent)
    {
        WithdrawCashController controller = new WithdrawCashController();
        controller.index();
    }

    @FXML
    public void onTransferMoneyButtonClick(MouseEvent mouseEvent)
    {
        //
    }

    @FXML
    public void onPaymentHistoryButtonClick(MouseEvent mouseEvent)
    {
        //
    }

    @FXML
    public void onAccountBalanceButtonClick(MouseEvent mouseEvent)
    {
        BalanceController controller = new BalanceController();
        controller.index();
    }

    @FXML
    public void onAccountSettingsButtonClick(MouseEvent mouseEvent)
    {
        //
    }

    @FXML
    public void onExitAndReturnCardButtonClick(MouseEvent mouseEvent)
    {
        exitAndReturnCard();
    }
}
