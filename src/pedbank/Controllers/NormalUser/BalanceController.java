package pedbank.Controllers.NormalUser;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import pedbank.App;
import pedbank.Controllers.BaseController;
import pedbank.Controllers.WelcomeController;
import pedbank.Microcontroller;
import pedbank.Models.Account;
import pedbank.Models.Log;
import pedbank.Models.User;
import pedbank.View;

import java.net.URL;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.ResourceBundle;

public class BalanceController extends BaseController implements Initializable
{
    private final static String BACK_KEY = "C";
    private final static String EXIT_KEY = "D";

    private App app = App.getInstance();

    @FXML
    private Label userNameLabel;

    @FXML
    private Label account1IbanLabel;

    @FXML
    private Label account2TitleLabel;

    @FXML
    private Label account2IbanLabel;

    @FXML
    private Label balanceLabel;

    public void index()
    {
        Platform.runLater(() -> {
            app.sceneSwitcher.setController(this);
            app.sceneSwitcher.render(new View("NormalUser/Balance"));
        });
    }

    @Override
    public void handleSerialData(String data)
    {
        if (!data.startsWith(Microcontroller.KEYPAD_PREFIX)) {
            return;
        }

        String enteredChar = data.replace(Microcontroller.KEYPAD_PREFIX, "");
        enteredChar = enteredChar.replace(System.getProperty("line.separator"), "");

        switch (enteredChar) {
            case BACK_KEY:
                Log.createFromMessage("[View:Balance][Client] -> [View:ViewSelection]");
                ViewSelectionController viewSelectionController = new ViewSelectionController();
                viewSelectionController.index();
            break;

            case EXIT_KEY:
                Log.createFromMessage("[View:Balance][Client] Quit session.");
                WelcomeController welcomeController = new WelcomeController();
                welcomeController.index();
            break;
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        User user = app.card.user;
        String name = user.initials + " " + user.surname;
        userNameLabel.setText(name);

        LinkedHashSet<Account> accounts = app.card.accounts;
        Iterator<Account> accountIterator = accounts.iterator();
        account1IbanLabel.setText(accountIterator.next().iban);

        if (accountIterator.hasNext()) {
            account2IbanLabel.setText(accountIterator.next().iban);
            account2TitleLabel.setDisable(false);
            account2IbanLabel.setDisable(false);
        }

        balanceLabel.setText("€" + app.selectedAccount.balance);
    }

    @FXML
    public void onBackButtonClick(MouseEvent mouseEvent)
    {
        ViewSelectionController controller = new ViewSelectionController();
        controller.index();
    }

    @FXML
    public void onExitAndReturnCardButtonClick(MouseEvent mouseEvent)
    {
        exitAndReturnCard();
    }
}
