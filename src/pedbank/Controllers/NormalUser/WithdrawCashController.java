package pedbank.Controllers.NormalUser;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import pedbank.Controllers.BaseController;
import pedbank.Controllers.WelcomeController;
import pedbank.Exceptions.WithdrawException;
import pedbank.Microcontroller;
import pedbank.Models.Account;
import pedbank.Models.Log;
import pedbank.Models.User;
import pedbank.View;

import java.net.URL;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.ResourceBundle;

public class WithdrawCashController extends BaseController implements Initializable
{
    private final static String CONFIRM_KEY = "A";
    private final static String CLEAR_KEY = "B";
    private final static String BACK_KEY = "C";
    private final static String EXIT_KEY = "D";

    @FXML
    private Label userNameLabel;

    @FXML
    private Label account1IbanLabel;

    @FXML
    private Label account2TitleLabel;

    @FXML
    private Label account2IbanLabel;

    @FXML
    TextField withdrawalAmountField;

    @FXML
    Label noteLabel;

    public void index()
    {
        Platform.runLater(() -> {
            app.sceneSwitcher.setController(this);
            app.sceneSwitcher.render(new View("NormalUser/WithdrawCash"));
        });
    }

    @Override
    public void handleSerialData(String data)
    {
        if (!data.startsWith(Microcontroller.KEYPAD_PREFIX)) {
            return;
        }

        String enteredChar = data.replace(Microcontroller.KEYPAD_PREFIX, "");
        enteredChar = enteredChar.replace(System.getProperty("line.separator"), "");

        if (enteredChar.matches("[0-9]")) {
            String newWithdrawalText = withdrawalAmountField.getText() + enteredChar;

            Platform.runLater(() -> {
                withdrawalAmountField.setText(newWithdrawalText);
            });
            return;
        }

        if (enteredChar.equals(CONFIRM_KEY)) {
            Log.createFromMessage("[View:Withdraw][Client] Withdrawing...");

            int withdrawalAmount = Integer.parseInt(withdrawalAmountField.getText());

            try {
                withdraw(withdrawalAmount);
            } catch (WithdrawException e) {
                Log.createFromMessage("[View:Withdraw][Server] Failed withdrawal.");

                Platform.runLater(() -> {
                    noteLabel.setText(e.getMessage());
                });
            }
        }

        switch (enteredChar) {
            case BACK_KEY:
                Log.createFromMessage("[View:Withdraw][Client] -> [View:ViewSelection]");

                ViewSelectionController viewSelectionController = new ViewSelectionController();
                viewSelectionController.index();
            break;

            case CLEAR_KEY:
                Log.createFromMessage("[View:Withdraw][Client] Cleared withdrawal field.");

                Platform.runLater(() -> {
                    withdrawalAmountField.setText("");
                });
            break;

            case EXIT_KEY:
                Log.createFromMessage("[View:Withdraw][Client] Quit session.");

                WelcomeController welcomeController = new WelcomeController();
                welcomeController.index();
            break;
        }
    }

    public void withdraw(int withdrawalAmount) throws WithdrawException
    {
        app.selectedAccount.withdraw(withdrawalAmount);

        Log.createFromMessage("[View:Withdraw][Client] Withdrawal succeeded.");

        showThankYouView();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        User user = app.card.user;
        String name = user.initials + " " + user.surname;
        userNameLabel.setText(name);

        LinkedHashSet<Account> accounts = app.card.accounts;
        Iterator<Account> accountIterator = accounts.iterator();
        account1IbanLabel.setText(accountIterator.next().iban);

        if (accountIterator.hasNext()) {
            account2IbanLabel.setText(accountIterator.next().iban);
            account2TitleLabel.setDisable(false);
            account2IbanLabel.setDisable(false);
        }

        noteLabel.setText("");
    }

    @FXML
    public void onWithdrawButtonClick(MouseEvent mouseEvent)
    {
        int withdrawalAmount = Integer.parseInt(withdrawalAmountField.getText());

        try {
            withdraw(withdrawalAmount);
        } catch (WithdrawException e) {
            Platform.runLater(() -> {
                noteLabel.setText(e.getMessage());
            });
        }
    }

    @FXML
    public void onClearButtonClick(MouseEvent mouseEvent)
    {
        withdrawalAmountField.setText("");
    }

    @FXML
    public void onBackButtonClick(MouseEvent mouseEvent)
    {
        ViewSelectionController controller = new ViewSelectionController();
        controller.index();
    }

    public void onExitAndReturnCardButtonClick(MouseEvent mouseEvent)
    {
        exitAndReturnCard();
    }
}
