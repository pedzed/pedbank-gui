package pedbank.Controllers;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import pedbank.Exceptions.ModelNotFoundException;
import pedbank.Microcontroller;
import pedbank.Models.Card;
import pedbank.Models.Log;
import pedbank.View;

import java.net.URL;
import java.util.ResourceBundle;

public class WelcomeController extends BaseController implements Initializable
{
    @FXML
    private Label noteLabel;

    public void index()
    {
        Platform.runLater(() -> {
            app.sceneSwitcher.setController(this);
            app.sceneSwitcher.render(new View("Welcome"));
        });
    }

    public void handleSerialData(String data)
    {
        if (!data.startsWith(Microcontroller.RFID_PREFIX)) {
            return;
        }

        String enteredCardId = data.replace(Microcontroller.RFID_PREFIX, "");
        enteredCardId = enteredCardId.replace(System.getProperty("line.separator"), "");

        try {
            app.card = Card.find(enteredCardId);

            showAuthView();
        } catch (ModelNotFoundException e) {
            Log.createFromMessage("[View:Welcome] Card '" + enteredCardId + " not found.");

            Platform.runLater(() -> {
                noteLabel.setText(e.getMessage() + " Please, try again.");
            });
        }
    }

    private void showAuthView()
    {
        Log.createFromMessage("[View:Welcome][Client] -> [VIEW:Auth]");

        AuthController controller = new AuthController();
        controller.index();
    }

    @FXML
    public void onKeyReleased(KeyEvent keyEvent)
    {
        if (keyEvent.getCode() == KeyCode.SPACE) {
            showAuthView();
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        noteLabel.setText("");
    }
}
