# PedBank

_The bank of Ped._


## Documentation

### EER Diagram

![EER Diagram](docs/eer-diagram.png)


## GUI previews

![Welcome screen](images/welcome.png)
_The welcome screen._

![Auth screen](images/auth.png)
_The authentication screen._

![Menu screen](images/selection.png)
_The menu screen._

![Balance screen](images/balance.png)
_The balance screen._

![Withdraw screen](images/withdraw.png)
_The withdraw screen._

![Success screen](images/success.png)
_The success screen._
